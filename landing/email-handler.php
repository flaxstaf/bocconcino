<?php
$from = 'info@bocconcino.ru';
$to = array('admin@bocconcino.ru');

// Array of names of the fileds that are not suppose to be in message body
$e_body_content_filter = array('subject'); 

$boundary = md5(time());
$mail_attachment = $_FILES['file'];
$subject = '=?UTF-8?B?' . base64_encode($_POST['subject']) . '?=';

$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= "Content-Type: multipart/mixed; boundary = $boundary\r\n";
$headers .= 'From: ' . $from . "\r\n";

$e_meta = '<meta name="viewport" content="width=device-width" /> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
$e_title = '<title>Simple Transactional Email</title>';
$e_styles = '<style>img{border:none;-ms-interpolation-mode:bicubic;max-width:100%}body{background-color:#f6f6f6;font-family:sans-serif;-webkit-font-smoothing:antialiased;font-size:14px;line-height:1.4;margin:0;padding:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}table{border-collapse:separate;mso-table-lspace:0;mso-table-rspace:0;width:100%}table td{font-family:sans-serif;font-size:14px;vertical-align:top}.body{background-color:#f6f6f6;width:100%}.container{display:block;margin:0 auto!important;max-width:580px;padding:10px;width:580px}.content{box-sizing:border-box;display:block;margin:0 auto;max-width:580px;padding:10px}.main{background:#fff;border-radius:3px;width:100%}.wrapper{box-sizing:border-box;padding:20px}.content-block{padding-bottom:10px;padding-top:10px}.footer{clear:both;margin-top:10px;text-align:center;width:100%}.footer a,.footer p,.footer span,.footer td{color:#999;font-size:12px;text-align:center}h1,h2,h3,h4{color:#000;font-family:sans-serif;font-weight:400;line-height:1.4;margin:0;margin-bottom:30px}h1{font-size:35px;font-weight:300;text-align:center;text-transform:capitalize}ol,p,ul{font-family:sans-serif;font-size:14px;font-weight:400;margin:0;margin-bottom:15px}ol li,p li,ul li{list-style-position:inside;margin-left:5px}a{color:#3498db;text-decoration:underline}.btn{box-sizing:border-box;width:100%}.btn>tbody>tr>td{padding-bottom:15px}.btn table{width:auto}.btn table td{background-color:#fff;border-radius:5px;text-align:center}.btn a{background-color:#fff;border:solid 1px #3498db;border-radius:5px;box-sizing:border-box;color:#3498db;cursor:pointer;display:inline-block;font-size:14px;font-weight:700;margin:0;padding:12px 25px;text-decoration:none;text-transform:capitalize}.btn-primary table td{background-color:#3498db}.btn-primary a{background-color:#3498db;border-color:#3498db;color:#fff}.last{margin-bottom:0}.first{margin-top:0}.align-center{text-align:center}.align-right{text-align:right}.align-left{text-align:left}.clear{clear:both}.mt0{margin-top:0}.mb0{margin-bottom:0}.preheader{color:transparent;display:none;height:0;max-height:0;max-width:0;opacity:0;overflow:hidden;mso-hide:all;visibility:hidden;width:0}.powered-by a{text-decoration:none}hr{border:0;border-bottom:1px solid #f6f6f6;margin:20px 0}@media only screen and (max-width:620px){table[class=body] h1{font-size:28px!important;margin-bottom:10px!important}table[class=body] a,table[class=body] ol,table[class=body] p,table[class=body] span,table[class=body] td,table[class=body] ul{font-size:16px!important}table[class=body] .article,table[class=body] .wrapper{padding:10px!important}table[class=body] .content{padding:0!important}table[class=body] .container{padding:0!important;width:100%!important}table[class=body] .main{border-left-width:0!important;border-radius:0!important;border-right-width:0!important}table[class=body] .btn table{width:100%!important}table[class=body] .btn a{width:100%!important}table[class=body] .img-responsive{height:auto!important;max-width:100%!important;width:auto!important}}@media all{.ExternalClass{width:100%}.ExternalClass,.ExternalClass div,.ExternalClass font,.ExternalClass p,.ExternalClass span,.ExternalClass td{line-height:100%}.apple-link a{color:inherit!important;font-family:inherit!important;font-size:inherit!important;font-weight:inherit!important;line-height:inherit!important;text-decoration:none!important}#MessageViewBody a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit}.btn-primary table td:hover{background-color:#34495e!important}.btn-primary a:hover{background-color:#34495e!important;border-color:#34495e!important}}</style>';

$e_template_head = '<!doctype html><html><head>' . $e_meta . $e_title . $e_styles . '</head>';
$e_body = '<p>Поступил новая завяка от пользователя.</p><br />';
$e_template_start = '<body class=""><table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body"><tr><td>&nbsp;</td><td class="container"><div class="content"><table role="presentation" class="main"><tr><td class="wrapper"><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td>';
$e_template_end = '</td></tr></table></td></tr></table><div class="footer"><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="content-block"> <span class="apple-link">© 2019. Сеть ресторанов «Bocconcino»</span></td></tr></table></div></div></td><td>&nbsp;</td></tr></table></body></html>';

$e_body_content = '';
foreach ($_POST as $field_name => $field_value) {

	if (in_array($field_name, $e_body_content_filter)) { 
		continue;
	}

	$e_body_content .= '<p>' . '<strong>' . $field_name . ':</strong> ' . $field_value . '</p>';
}

$body .= "--$boundary\r\n";
$body .= 'Content-Type: text/html; charset=UTF-8' . "\r\n";
$body .= 'Content-Transfer-Encoding: 8bit' . "\r\n\r\n";

$body .= $e_template_head . $e_template_start . $e_body . $e_body_content . $e_template_end;

if (!empty($mail_attachment)) {
	$tmp_name = $mail_attachment['tmp_name'];
	$file_size = $mail_attachment['size'];
	$file_name = $mail_attachment['name'];
	$file_type = $mail_attachment['type'];

	$handle = fopen($tmp_name, "r");
	$content = fread($handle, $file_size);
	fclose($handle);
	$encoded_content = chunk_split(base64_encode($content)); 

	//attachment 
	$body .= "\r\n\r\n--$boundary\r\n";
	$body .= "Content-Type: $file_type; name=".$file_name."\r\n"; 
	$body .= "Content-Disposition: attachment; filename=".$file_name."\r\n"; 
	$body .= "Content-Transfer-Encoding:base64\r\n";
	$body .= "X-Attachment-Id: ".rand(1000, 99999)."\r\n\r\n"; 
	$body .= "$encoded_content\r\n";
	$body .= "--$boundary--\r\n";
}

$mail_recipients = implode(',', $to);
$mail_status = mail($mail_recipients, $subject, $body, $headers);

if ($mail_status) {
	$output = array('status' => 'success', 'message' => 'Ваша заявка была успешно отправлена');
}
else {
	$output = array('status' => 'error', 'message' => 'Не удалось отправить заявку');
}

echo json_encode($output);