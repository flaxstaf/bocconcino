jQuery(function ($) {

	$('.scroll').click(function(event) {
		smooth_scroll($(this), false, 50);
	});
	$(document).ready(function() { 
		if(window.location.hash) {
			hashname = window.location.hash.replace('#', '');
			history.pushState("", document.title, window.location.pathname + window.location.search);
			elem = $('#' + hashname);
			if(hashname.length > 1) {
				smooth_scroll(elem, true)
			}
		}
	});
	function smooth_scroll(block, is_string, topper = 120) {
		if (is_string) {
			$('html, body').stop().animate({
				scrollTop: $(block).offset().top - topper
			}, 1500, 'easeInOutCubic');
		} else {
			var anchor = (block.attr('data-target')) ? block.attr('data-target') : block.attr('href');
			$('html, body').stop().animate({
				scrollTop: $(anchor).offset().top - topper
			}, 1500, 'easeInOutCubic');
		}
	}

	$(window).load(function() {
		$(".navbar-custom.sticky").sticky({topSpacing:0});
	});

	$('.location-tab_btn').on('shown.bs.tab', function (e) {
		if ($($(this).attr('data-target')).length && $(window).width() < 768) {
			smooth_scroll($(this));
		}
	})

	$('.navbar-custom .navbar-toggler').on( 'click', function() {
		$(this).toggleClass('active');
	});

	if ($('body').find('.skitter').length > 0) {
		$(document).ready(function() { 
			$('.skitter').skitter({
				fullscreen: false,
				interval: 5000,
				navigation: false,
				dots: false,
				with_animations: [
				"cube", "cubeRandom", "block", "horizontal", "showBars", "showBarsRandom", "tube", "fade", "fadeFour",
				"paralell", "blind", "blindHeight", "blindWidth", "directionRight", "directionLeft", "cubeSpread", "glassCube", 
				"glassBlock", "circles", "circlesInside", "circlesRotate", "cubeShow", "swapBars", "swapBarsBack", "swapBlocks", "cut"
				],
				responsive: {
					small: {
						max_width: 480,
						suffix: '-small'
					},
					medium: {
						max_width: 800,
						suffix: '-medium'
					}
				}
			});
		});
	}

	$('.navbar_scart').on('click', '.navbar_scart--btn_search', function() {
		$(this).parents('.navbar_scart').toggleClass('search_active');
	});

	$('.vediocall_play-button').on('click', function() {
		var container = $('.vediocall_wrap');

		container.toggleClass('vediocall--play');
		container.find('iframe')[0].contentWindow.postMessage('{"event":"command","func":"' + 'playVideo' + '","args":""}', '*');
	});
	

	$(document).ready(function() { 
		$('.welcome_banner_control').on('click', 'button', function() {
			var btn = $(this),
				slickContainer = btn.parents('section').find('.skitter');

			if (btn.hasClass('prev')) {
				slickContainer.skitter('prev');
			} else {				
				slickContainer.skitter('next');
			}
		});
	});
	
	if ($('body').find('.easyzoom').length > 0 && $(window).width() > 768) {
		$('.easyzoom').easyZoom();
	}

	// $('.phone-mask').mask('+7 (000) 000-00-00', {
	// 	/* onKeyPress: function(cep, e, field, options) {
	// 		var masks = ['+00 (000) 000-00-00', '(000) 000-00-00'];
	// 		var mask = (cep.length >= 15) ? masks[0] : masks[1];
	// 		$('.phone-mask').mask(mask, options);
	// 	} */
	// });
	$(".phone-mask").mask("+7 (999) 999-99-99");

	$('.bselect').selectpicker({
		hideDisabled: false
	});
	
	$('.location-item_slides--slick_handler').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		autoplay: false,
		autoplaySpeed: 3000
	});

	$('.banquet-gallery_nav--slick_handler').slick({
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 1,
		arrows: true,
		autoplay: false,
		responsive: [
			{
				breakpoint: 800,
				settings: {
					slidesToShow: 3
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					autoplay: true
				}
			}
		]
	});	

	$('.partners--slick_handler').slick({
		infinite: true,
		slidesToShow: 6,
		slidesToScroll: 1,
		arrows: true,
		dots: true,
		autoplay: false,
		responsive: [
			{
				breakpoint: 1000,
				settings: {
					slidesToShow: 4,
					dots: false,
					autoplay: true
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 2,
					dots: false,
					autoplay: true
				}
			}
		]
	});	

	$('.places--slick_handler').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		dots: false,
		autoplay: false
	});	

	$('.fixed_tabs').on('click', '[data-toggle="tab"]', function(){
		var container = $('.fixed_tabs'),
			navButton = $(this),
			tabTarget = navButton.attr('data-target'),
			navContainer = container.find('.nav-tabs'),
			tabContainer = container.find('.tab-content'),
			navItems = navContainer.find('[data-toggle="tab"]'),
			tabItems = tabContainer.find('.tab-pane');
			
		
		navItems.removeClass('active show');
		tabItems.removeClass('active show');

		navButton.addClass('active show');
		$(tabTarget).addClass('active');

		setTimeout(function() {
			$(tabTarget).addClass('show');
		}, 0);
	});

	$('.location_tabs button[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		var tab = $(e.target).attr('data-target');
     	$(tab).find('.slick-slider').slick('setPosition');
	})

	$('.testimonials_list--slick_handler').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		dots: true,
		autoplay: false,
		autoplaySpeed: 3000
	});

	$('.mainews_list--slick_handler').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 3,
		arrows: true,
		dots: true,
		autoplay: true,
		autoplaySpeed: 3000,
		responsive: [
			{
				breakpoint: 1195,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
					arrows: false,
					dots: false
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: false,
					dots: false
				}
			}
		]
	});

	$(document).ready(function() { 
		var controlPanels = $('.slick_custom-control');
		
		controlPanels.each(function() {
			var controlPanel = $(this),
				slider = controlPanel.parents('section').find('.slick-slider');
			
				console.log(controlPanel.find('.prev'));
			slider.slick('slickSetOption', {
				prevArrow: controlPanel.find('.prev'),
				appendDots: controlPanel.find('.dots'),
				nextArrow: controlPanel.find('.next')
			}, true);
		});
	});

	$('.product-price_control').on('click', 'button', function() {
		var container = $(this).parents('.product-price_control'),
			is_plus = $(this).hasClass('plus'),
			addend = (is_plus) ? 1 : -1,
			quantity = container.find('input'),
			quantity_val = parseInt(quantity.val()),
			quantity_num = (is_plus) ? quantity_val + 1 : quantity_val - 1,
			quantity_min = quantity.attr('min'),
			quantity_max = quantity.attr('max');

		if(quantity_num <= quantity_max && quantity_num >= quantity_min) {
			quantity.val(quantity_val + addend);
			quantity.trigger('change');
		}
	}).on('change', 'input', function() {
		var itemContainer = $(this).parents('.product--controller'),
			itemPrice = itemContainer.attr('data-item_price');

		itemContainer.find('.product-price_num span').text($(this).val() * itemPrice);
	});
	
	$('.contact_location--slick_handler').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		autoplay: false,
		autoplaySpeed: 3000,
		responsive: [
			{
				breakpoint: 768,
				settings: {
					arrows: true
				}
			}
		]
	});

	$('.about_item--slick_handler').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		autoplay: false,
		autoplaySpeed: 3000
	});

	$('.delivery_list--slick_handler').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		autoplay: false,
		autoplaySpeed: 3000
	});

	$(document).ready(function() { 
		var Shuffle = window.Shuffle,
			navContainer = $('.custom-shuffle_nav'),
			listContainer = $('.custom-shuffle_list'),
			shuffleItemClass = '.custom-shuffle_item';

		if (listContainer.length < 1) {
			return;
		}

		var shuffleInstance = new Shuffle(listContainer, {
			itemSelector: shuffleItemClass
		});

		navContainer.on('click', 'button', function () {
			$('.custom-shuffle_nav').find('button').removeClass('active');
			$(this).addClass('active');
			var filter = $(this).attr('data-filter');
			shuffleInstance.filter(filter);
		});
	});

	if ($('body').find('.pswiper').length > 0) {
		$('.pswiper').photoSwipe(
			'.slide', //selector
			{ //options
				bgOpacity: 0.8,
				history: false,	
			},
			{ //events
				close: function() {
					$('.pswp .videowrapper').remove();
				}
			}
		);
	}

	$('.custom_form-control_file input').on('change', function(){ 
		var filename = $(this).val().split('\\').pop(),
			infoblock = $(this).parents('form').find('.custom_form-control_file-info');

		infoblock.find('span').text(filename)
	});
	
	$('.modal_reserv-action').on('click', function() {
		var btn = $(this),
			reservModalContainer = $('#modalTableReserv'),
			reservModalSelect = reservModalContainer.find('select[name="location"]');
		
		reservModalContainer.modal('show');

		if (restname = btn.attr('data-restname')) {
			reservModalSelect.val(restname);
			if (reservModalSelect.parents('.bootstrap-select').length) { 
				reservModalSelect.selectpicker('refresh');
			}
		}
	});

	// Cart iteractions
	$(document).ready(function() { 
		$('.checkout_delivery input[name="faster"]').on('change', function () {
			var selecter = $(this).parents('.checkout_delivery').find('select[name="delivery-time"]');
			if ($(this).is(":checked")) {
				selecter.prop("disabled", true);
			} else {
				selecter.prop("disabled", false);
			}
			selecter.selectpicker('refresh');
		});

		$('.checkout_delivery input[type=radio][name=payment-type]').on('change', function () {
			if ($(this).val() == 'cash') {
				$(this).parents('.custom_form_group').next().show();
			} else {
				$(this).parents('.custom_form_group').next().hide();
			}
		});
	});

	if ($('body').find('#map-item').length > 0) {
		ymaps.ready(function() {
			var mapContainer = $('#map-item'),
				suggestContainer = $('.map_finder'),
				selecterContainer = $('.map_marker-setter_container'),
				cafesListContainer = $('.cafes_list');
			
			var yaMap = new ymaps.Map('map-item', {
				center: [55.750586, 37.609075],
				zoom: 10,
				controls: ["fullscreenControl", "zoomControl"]
			});

			var objectManager = new ymaps.ObjectManager({
				clusterize: false
			});
			yaMap.geoObjects.add(objectManager);

			if (suggestContainer.length > 0) {
				var searchItem = suggestContainer.find('input'),
					suggestView = new ymaps.SuggestView(searchItem.attr('id'));
			}

			if (cafesListContainer.length > 0) {
				var cafes = createCafesCollection(cafesListContainer);
				addGeoObject(objectManager, cafes);
			} else {
				addGeoObject(objectManager, '../map-cafes.json');
				addGeoObject(objectManager, '../map-zones.json');
			}

			objectManager.objects.events.add(['click'], function (e) {
				var objectId = e.get('objectId');
				//console.log(objectId);
			});

			if (selecterContainer.length > 0) {
				var selecterChooser = selecterContainer.find('select.map_marker-setter_select'),
					selecterProceed = selecterContainer.find('.map_marker-setter_action');
				
				selecterProceed.on('click', function () {
					var selecterCoordinates = selecterChooser.val();

					if (selecterCoordinates !== '') {
						setPosition(selecterCoordinates, mapContainer, yaMap);
					}
				});
			}

			$('.map_finder-search').on('click', function () {
				proccessUserAdress(yaMap, cafes);
			});
			
			$('.contact_action-map').on('click', function () {
				var item = $(this).parents('.contact_location'),
					coordinates = item.attr('data-coordinates');
				
				setPosition(coordinates, mapContainer, yaMap)
			});
			
			function setPosition(coordinates, mapContainer, yaMap) {
				ymaps.geocode(coordinates).then(function (res) {
					var location = res.geoObjects.get(0),
						centerPosition = ymaps.util.bounds.getCenterAndZoom( location.properties.get('boundedBy'), [mapContainer.width(), mapContainer.height()] );

					yaMap.setCenter(centerPosition.center, centerPosition.zoom);
				});
			}

			function proccessUserAdress(yaMap, cafes) {
				var searchResult = searchItem.val();
				ymaps.geocode(searchResult).then(function (res) {
					var location = res.geoObjects.get(0),
						precision = location.properties.get('metaDataProperty.GeocoderMetaData.precision'),
						error = (precision == 'exact') ? false : true;

					if (error) {
						searchContainer.addClass('error').attr('data-err-message', 'Адрес не найден, уточните адрес');
						setTimeout(function() {
							searchContainer.removeClass('error').attr('data-err-message', '');
						}, 5000);
					} else {
						var userPosition = location.geometry._coordinates.join(', '),	
							closest = findClosestCoordinates(cafes, userPosition);
						
						createRoute(yaMap, userPosition, closest);
					}
				})
		
			}

			function createRoute(yaMap, start, destination) {
				yaMap.controls.add('routePanelControl', {
					maxWidth: 300,
				});
				var routePanel = yaMap.controls.get('routePanelControl').routePanel,
					destination = (destination) ? destination : '55.743692, 37.539800';
				
				console.log(start, destination);

				routePanel.options.set('adjustMapMargin', true);
				routePanel.state.set({
					allowSwitch: true,
					fromEnabled: true,
					toEnabled: false,
					from: start,
					to: destination,
					type: "auto"
				});
			}

			function createPlacemark(yaMap, coordinates, hint = false) {
				var coordinates = coordinates.split(', '),
					hint = (hint) ? hint : '';

				yaMap.geoObjects.add(new ymaps.Placemark(coordinates, {
					hintContent: hint
				}, {
					preset: 'islands#brownDotIcon'
				}));
			}

			function findClosestCoordinates(collection, coordinates) {
				var geoObject = createGeoObject(collection);
				return geoObject.getClosestTo(coordinates.split(', ')).geometry._coordinates.join(', ');
			}

			function createCafesCollection(container) {
				var cafes = container.find('.contact_location'),
					cafes_list = [],
					id = 0;

				cafes.each(function() {
					var coordinates = $(this).attr('data-coordinates'),
						hint = $(this).find('h3').text(),
						balloonContentBody = '<div class="contact_location-content">' + $(this).find('.contact_location-content').html().replace(/(\r\n|\n|\r)/gm, "") + '</div>';
					
					var cafe_item = {
						type: 'Feature',
						id: 'htm' + id,
						geometry: {
							type: 'Point',
							coordinates: coordinates.split(', ')
						},
						properties: {
							hintContent: hint,
							balloonContentBody: balloonContentBody
						}
					}

					cafes_list.push(cafe_item);
					id++;
				});
				
				cafes = {
					type: 'FeatureCollection',
					features: cafes_list
				};

				return cafes;
			}

			function addGeoObject(objectManager, item) {
				if (Object.prototype.toString.call(item) == '[object String]' && item.length) {
					$.getJSON(item, function(json) {
						objectManager.add(json);
					});
				} else {
					objectManager.add(item);
				}
			}

			function createGeoObject(obj) {
				return ymaps.geoQuery(obj);
			}

		});
	}
});
